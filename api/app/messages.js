const express = require('express');
const db = require('../fileDB');

const router = express.Router();


router.get('/', (req, res) => {
    const date = req.query.datetime;
    if (date) {
        if (isNaN(new Date(date).getDate())) {
            return res.status(400).send({message: 'Date (' + date + ') is incorrect'});
        }
        return res.send(db.getItemsByDate(date));
    }
    return res.send(db.getItems());
});

router.post('/', async (req, res, next) => {
    try {
        if (!req.body.message || !req.body.author) {
            return res.status(400).send({message: 'Author and message must be present in the request'});
        }
        const message = {
            message: req.body.message,
            author: req.body.author,
        }
        await db.addItem(message);

        return res.send({message: 'Created new message with ID=' + message.id});

    } catch (e) {
        next(e);
    }

});

module.exports = router;