import { Msg } from './msg.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class MsgService {
  messagesChange = new Subject<Msg[]>();
  messagesFetching = new Subject<boolean>();
  messagesPosting = new Subject<boolean>();

  idInterval = 0;

  private messages: Msg[] = [];

  constructor(private http: HttpClient) {}

  fetchMessages() {
    this.messagesFetching.next(true);
    this.http.get<{ [id: string]: Msg}>('http://localhost:8000/messages')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new Msg(data.message, data.author, id, data.datetime);
        });
      }))
      .subscribe(messages => {
        this.messages = messages;
        this.messagesChange.next(this.messages.slice());
        this.messagesFetching.next(false);
      }, () => {
        this.messagesFetching.next(false);
      });
  }

  start() {
    this.fetchMessages();
    this.idInterval = setInterval(() => {
      this.fetchMessages();
    }, 5000);
  }

  stop() {
    if (this.idInterval) {
      clearInterval(this.idInterval);
    }
  }

  post(body: Object) {
    this.messagesPosting.next(true);
    this.http.post('http://localhost:8000/messages', body).subscribe(() => {
      this.fetchMessages();
      this.messagesPosting.next(false);
    });
  }
}
