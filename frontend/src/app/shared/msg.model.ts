export class Msg {
  constructor(
    public message: string,
    public author: string,
    public id: string,
    public datetime: string,
  ) {}
}
