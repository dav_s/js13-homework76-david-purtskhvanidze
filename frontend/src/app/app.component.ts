import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MsgService } from './shared/msg.service';
import { Msg } from './shared/msg.model';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit, OnDestroy {
  @ViewChild('f') msgForm!: NgForm;

  messages: Msg[] = [];
  messagesChangeSubscription!: Subscription;
  messagesFetchingSubscription!: Subscription;
  messagesPostingSubscription!: Subscription;
  isFetching = false;
  isPosting = false;

  constructor(private msgService: MsgService) { }

  ngOnInit() {
    this.messagesChangeSubscription = this.msgService.messagesChange.subscribe((msg: Msg[]) => {
      this.messages = msg;
    });

    this.messagesFetchingSubscription = this.msgService.messagesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });

    this.messagesPostingSubscription = this.msgService.messagesPosting.subscribe((isPosting: boolean) => {
      this.isPosting = isPosting;
    });

    this.msgService.start();
  }

  postMsg() {
    const body = {
      message: this.msgForm.value.message,
      author: this.msgForm.value.author
    };

    this.msgService.post(body);
  }

  ngOnDestroy() {
    this.msgService.stop();
    this.messagesChangeSubscription.unsubscribe();
    this.messagesFetchingSubscription.unsubscribe();
    this.messagesPostingSubscription.unsubscribe();
  }
}

